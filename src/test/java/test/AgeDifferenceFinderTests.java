package test;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ageDifferenceAlgorithm.*;
import ageDifferenceAlgorithm.enums.Criteria;
import ageDifferenceAlgorithm.errors.InvalidCriteriaType;
import org.junit.Before;
import org.junit.Test;

public class AgeDifferenceFinderTests {

	Person sue = new Person();
	Person greg = new Person();
	Person sarah = new Person();
	Person mike = new Person();

	@Before
	public void setup() {
		sue.setName("Sue");
		sue.setBirthDate(new Date(50, 0, 1));

		greg.setName("Greg");
		greg.setBirthDate(new Date(52, 5, 1));

		sarah.setName("Sarah");
		sarah.setBirthDate(new Date(82, 0, 1));

		mike.setName("Mike");
		mike.setBirthDate(new Date(79, 0, 1));
	}

	@Test
	public void ReturnsEmptyResultsWhenGivenEmptyList() throws InvalidCriteriaType {
		List<Person> list = new ArrayList<Person>();
		AgeDifferenceFinder ageDifferenceFinder = new AgeDifferenceComparatorImp().makeFinder(Criteria.SmallestAgeDifference, list);

		AgeDifferenceBetweenTwoPeople result = ageDifferenceFinder.find();
		assertEquals(null, result.getYounger());

		assertEquals(null, result.getOlder());
	}

	@Test
	public void ReturnsEmptyResultsWhenGivenOnePerson() throws InvalidCriteriaType {
		List<Person> list = new ArrayList<Person>();
		list.add(sue);

		AgeDifferenceFinder ageDifferenceFinder =  new AgeDifferenceComparatorImp().makeFinder(Criteria.SmallestAgeDifference, list);

		AgeDifferenceBetweenTwoPeople result = ageDifferenceFinder.find();

		assertEquals(null, result.getYounger());
		assertEquals(null, result.getOlder());
	}

	@Test
	public void ReturnsClosestTwoForTwoPeople() throws InvalidCriteriaType {
		List<Person> list = new ArrayList<Person>();
		list.add(sue);
		list.add(greg);
		AgeDifferenceFinder ageDifferenceFinder =  new AgeDifferenceComparatorImp().makeFinder(Criteria.SmallestAgeDifference, list);

		AgeDifferenceBetweenTwoPeople result = ageDifferenceFinder.find();

		assertEquals(sue, result.getYounger());
		assertEquals(greg, result.getOlder());
	}

	@Test
	public void ReturnsFurthestTwoForTwoPeople() throws InvalidCriteriaType {
		List<Person> list = new ArrayList<Person>();
		list.add(mike);
		list.add(greg);

		AgeDifferenceFinder ageDifferenceFinder =  new AgeDifferenceComparatorImp().makeFinder(Criteria.BiggestAgeDifference, list);

		AgeDifferenceBetweenTwoPeople result = ageDifferenceFinder.find();

		assertEquals(greg, result.getYounger());
		assertEquals(mike, result.getOlder());
	}

	@Test
	public void ReturnsFurthestTwoForFourPeople() throws InvalidCriteriaType {
		List<Person> list = new ArrayList<Person>();
		list.add(sue);
		list.add(sarah);
		list.add(mike);
		list.add(greg);
		AgeDifferenceFinder ageDifferenceFinder =  new AgeDifferenceComparatorImp().makeFinder(Criteria.BiggestAgeDifference, list);

		AgeDifferenceBetweenTwoPeople result = ageDifferenceFinder.find();

		assertEquals(sue, result.getYounger());
		assertEquals(sarah, result.getOlder());
	}

	@Test
	public void ReturnsClosestTwoForFourPeople() throws InvalidCriteriaType {
		List<Person> list = new ArrayList<Person>();
		list.add(sue);
		list.add(sarah);
		list.add(mike);
		list.add(greg);

		AgeDifferenceFinder ageDifferenceFinder =  new AgeDifferenceComparatorImp().makeFinder(Criteria.SmallestAgeDifference, list);

		AgeDifferenceBetweenTwoPeople result = ageDifferenceFinder.find();

		assertEquals(sue, result.getYounger());
		assertEquals(greg, result.getOlder());
	}

	@Test(expected = InvalidCriteriaType.class)
	public void ReturnsInvalidCriteriaType() throws InvalidCriteriaType {
		List<Person> list = new ArrayList<Person>();
		list.add(sue);
		list.add(sarah);
		list.add(mike);
		list.add(greg);

		AgeDifferenceFinder ageDifferenceFinder =  new AgeDifferenceComparatorImp().makeFinder(Criteria.InvalidCriteria, list);

	}

}
