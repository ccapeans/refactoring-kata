package ageDifferenceAlgorithm;

import java.util.List;

public class SmallestAgeDifferenceFinder extends AgeDifferenceFinder{
    public SmallestAgeDifferenceFinder(List<Person> people) {
        super(people);
    }

    public AgeDifferenceBetweenTwoPeople getAgeDifference(List<AgeDifferenceBetweenTwoPeople> ageDifferenceList) {
        AgeDifferenceBetweenTwoPeople smallestAgeDifference = ageDifferenceList.get(0);
        for (AgeDifferenceBetweenTwoPeople ageDifference : ageDifferenceList) {
            if (ageDifference.getAgeDifference() < smallestAgeDifference.getAgeDifference()) {
                smallestAgeDifference = ageDifference;
            }
        }
        return smallestAgeDifference;
    }

}
