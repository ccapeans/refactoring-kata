package ageDifferenceAlgorithm;

import java.util.Date;

public class Person {
    private String name;
    private Date birthDate;

    public boolean isYoungerThan(Person person){
        return this.birthDate.getTime() < person.birthDate.getTime();
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String toString(){
        return this.name + " " + "was born in: " + this.birthDate;
    }
}

