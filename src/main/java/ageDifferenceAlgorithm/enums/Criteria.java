package algorithm.enums;
public enum Criteria {
	SmallestAgeDifference, BiggestAgeDifference, InvalidCriteria
}
