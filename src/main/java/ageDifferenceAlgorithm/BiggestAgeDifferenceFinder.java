package ageDifferenceAlgorithm;

import java.util.List;

public class BiggestAgeDifferenceFinder extends AgeDifferenceFinder{
    public BiggestAgeDifferenceFinder(List<Person> people) {
        super(people);
    }

    public AgeDifferenceBetweenTwoPeople getAgeDifference(List<AgeDifferenceBetweenTwoPeople> ageDifferenceList) {
        AgeDifferenceBetweenTwoPeople biggestAgeDifference = ageDifferenceList.get(0);
        for (AgeDifferenceBetweenTwoPeople ageDifference : ageDifferenceList) {
            if (ageDifference.getAgeDifference() > biggestAgeDifference.getAgeDifference()) {
                biggestAgeDifference = ageDifference;
            }
        }
        return biggestAgeDifference;
    }
}
