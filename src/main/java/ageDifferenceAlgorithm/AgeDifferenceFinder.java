package ageDifferenceAlgorithm;

import java.util.ArrayList;
import java.util.List;

public abstract class AgeDifferenceFinder {
    private static final int FIRST = 0;

    private final List<Person> people;

    public AgeDifferenceFinder(List<Person> people) {
        this.people = people;
    }

    public AgeDifferenceBetweenTwoPeople find() {

        List<AgeDifferenceBetweenTwoPeople> ageDifferenceList = generateAgeDifferenceList();

        if (ageDifferenceList.isEmpty()) {
            return new AgeDifferenceBetweenTwoPeople();
        }

        return this.getAgeDifference(ageDifferenceList);
    }

    private List<AgeDifferenceBetweenTwoPeople> generateAgeDifferenceList() {
        List<AgeDifferenceBetweenTwoPeople> ageDifferenceList = new ArrayList<AgeDifferenceBetweenTwoPeople>();
        Person currentPerson, nextPerson;
        AgeDifferenceBetweenTwoPeople auxAgeDifference;

        for (int current = 0; current < people.size() - 1; current++) {
            for (int next = current + 1; next < people.size(); next++) {

                auxAgeDifference = new AgeDifferenceBetweenTwoPeople();

                currentPerson = this.people.get(current);
                nextPerson = this.people.get(next);

                auxAgeDifference.setYoungerAndOlder(currentPerson, nextPerson);

                auxAgeDifference.setAgeDifference();

                ageDifferenceList.add(auxAgeDifference);
            }
        }
        return ageDifferenceList;
    }

    abstract AgeDifferenceBetweenTwoPeople getAgeDifference(List<AgeDifferenceBetweenTwoPeople> ageDifferenceList);
}
