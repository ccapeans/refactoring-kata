package algorithm.errors;

public class InvalidCriteriaType extends Throwable{
    public InvalidCriteriaType(String message) {
        super(message);
    }
    public InvalidCriteriaType() {
        super();
    }
}
