package ageDifferenceAlgorithm;

public class AgeDifferenceBetweenTwoPeople {
    private Person younger;
    private Person older;
    private long ageDifference;

    public AgeDifferenceBetweenTwoPeople() {
        this.younger = null;
        this.older = null;
        this.ageDifference = 0;
    }

    public Person getYounger() {
        return younger;
    }

    public Person getOlder() {
        return older;
    }

    public long getAgeDifference() {
        return ageDifference;
    }

    public void setYoungerAndOlder(Person person1, Person person2) {
        if (person1.isYoungerThan(person2)) {
            this.younger = person1;
            this.older = person2;
        } else {
            this.younger = person2;
            this.older = person1;
        }
    }

    public void setAgeDifference() {
        this.ageDifference = this.older.getBirthDate().getTime() - this.younger.getBirthDate().getTime();
    }
}
