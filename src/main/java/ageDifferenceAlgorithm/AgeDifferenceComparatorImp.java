package ageDifferenceAlgorithm;

import ageDifferenceAlgorithm.enums.Criteria;
import ageDifferenceAlgorithm.errors.InvalidCriteriaType;
import ageDifferenceAlgorithm.interfaces.AgeDifferenceComparatorFactory;

import java.util.List;

public class AgeDifferenceComparatorImp implements AgeDifferenceComparatorFactory {
    public AgeDifferenceFinder makeFinder(Criteria criteria, List<Person> people) throws InvalidCriteriaType {
        switch (criteria) {
            case SmallestAgeDifference:
                return new SmallestAgeDifferenceFinder(people);
            case BiggestAgeDifference:
                return new BiggestAgeDifferenceFinder(people);
            default:
                throw new InvalidCriteriaType();
        }
    }
}
