package algorithm.interfaces;

import algorithm.AgeDifferenceFinder;
import algorithm.enums.Criteria;
import algorithm.Person;
import algorithm.errors.InvalidCriteriaType;

import java.util.List;

public interface AgeDifferenceComparatorFactory {
    public AgeDifferenceFinder makeFinder(Criteria criteria, List<Person> people) throws InvalidCriteriaType;
}

